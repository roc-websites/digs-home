(function($){
    "use strict";
    
    /* Preloader */
    $(window).load(function() {
        $('.loader').fadeOut();
        $('.page-loader').delay(350).fadeOut('slow');
    });

    /*---- STICKY NAVIGATION ---*/
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1){  
            $('header').addClass("sticky");
        }
        else{
            $('header').removeClass("sticky");
        }
    });
    
    
    /* Go up */
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 700) {
            jQuery(".go-up").css("bottom", "25px");
        } else {
            jQuery(".go-up").css("bottom", "-60px");
        }
    });
    jQuery(".go-up").click(function() {
        jQuery("html,body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });
    
})(jQuery); 